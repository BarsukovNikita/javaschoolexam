package com.tsystems.javaschool.tasks.calculator;


import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {

        if (!inputChecker(statement)) return null;

        String currentDigit = "";
        Stack<Double> values = new Stack<>();
        Stack<Character> operations = new Stack<>();
        int i = 0;

        try {
            for (char ch : statement.toCharArray()) {

                i++;
                if (ch == '(') {
                    operations.push('(');
                    continue;
                }
                if (ch == ')') {

                    values.push(Double.parseDouble(currentDigit));
                    currentDigit = "";

                    char operator = operations.pop();

                    while (operator != '(') {
                        values.push(operate(values.pop(), values.pop(), operator));

                        operator = operations.pop();
                    }

                    continue;
                }

                if (Character.isDigit(ch) || ch == '.')
                    currentDigit += ch;
                else {
                    if (operations.size() != 0) {

                    }
                    if (values.size() < 2) {
                        if (currentDigit != "") values.push(Double.parseDouble(currentDigit));
                        if (operations.size() == 0)
                            operations.push(ch);
                        else if ((getPrecedence(operations.peek()) < getPrecedence(ch)) && (operations.peek() != ('(') || operations.peek() != ')'))
                            operations.push(ch);
                        else {
                            if (operations.peek() == '(') {

                                operations.push(ch);
                                currentDigit = "";

                                continue;
                            }
                            values.push(operate(values.pop(), values.pop(), operations.pop()));
                            operations.push(ch);
                        }
                        currentDigit = "";
                    } else if (getPrecedence(operations.peek()) < getPrecedence(ch)) {

                        values.push(operate(values.pop(), values.pop(), operations.pop()));

                    } else {

                        if (operations.size() == 1 && values.size() == 2)
                            values.push(operate(values.pop(), values.pop(), operations.pop()));
                        else if (getPrecedence(operations.peek()) == getPrecedence(ch)) {

                            values.push(Double.parseDouble(currentDigit));
                            values.push(operate(values.pop(), values.pop(), operations.pop()));

                        } else values.push(Double.parseDouble(currentDigit));

                        operations.push(ch);
                        currentDigit = "";
                    }
                }

                if (i == statement.length()) {

                    if (Character.isDigit(ch))
                        values.push(Double.parseDouble(currentDigit));

                    while (values.size() != 1) {

                        values.push(operate(values.pop(), values.pop(), operations.pop()));

                    }

                    return values.pop().toString().replace(".0", "");

                }
            }

            return operate(values.pop(), values.pop(), operations.pop()).toString().replace(".0", "");

        } catch (RuntimeException e) {
            return null;
        }

    }

    private boolean inputChecker(String input) {

        if (input == null) return false;
        if (input.length() == 0) return false;
        Stack<Character> stack = new Stack<>();
        int count = 0;
        for (int i = 0; i < input.length(); i++) {


            if (input.contains(",")) return false;

            char ch = input.charAt(i);

            if (ch == '(') {
                stack.push('(');
                count++;
            }
            if (ch == ')') {
                count--;
                if (stack.size() == 0) return false;
                stack.pop();
            }

            if (ch == '(') {

                char nextChar = input.charAt(i + 1);

                if (nextChar == '(') continue;
                if (isOperator(nextChar)) return false;
                if (i != 0) {
                    char prevChar = input.charAt(i - 1);
                    if (Character.isDigit(prevChar) && i != 0) return false;
                }
            }


            if (ch == ')') {

                char prevChar = input.charAt(i - 1);


                if (input.length() - 1 != i) {
                    char nextChar = input.charAt(i + 1);
                    if (!isOperator(nextChar)) return false;
                }

                if (!Character.isDigit(prevChar)) return false;

            }

            if (isOperator(ch)) {
                if (i == 0) return false;
                if (i == input.length() - 1) return false;

                char prevChar = input.charAt(i - 1);
                char nextChar = input.charAt(i + 1);
                if (nextChar == '(') nextChar = '0';
                if (prevChar == ')') prevChar = '0';
                if (!Character.isDigit(prevChar) || (!Character.isDigit(nextChar))) return false;
            }


        }
        if (stack.size() == 0 || count == 0) return true;
        else return false;
    }

    private boolean isOperator(char ch) {
        switch (ch) {
            case '*':
            case '/':
            case '+':
            case '-':
                return true;
            default:
                return false;
        }
    }

    public int getPrecedence(char operator) {

        switch (operator) {
            case '(':
            case ')':
                return 3;
            case '*':
            case '/':
                return 2;
            case '+':
            case '-':
                return 1;
            default:
                return -1;
        }
    }

    private Double operate(Double a, Double b, char ch) {

        switch (ch) {

            case '*':
                return times(a, b);
            case '/':
                return div(a, b);
            case '+':
                return add(a, b);
            case '-':
                return sub(a, b);
            default:
                return null;

        }
    }

    private Double add(Double a, Double b) {
        return a + b;
    }

    private Double sub(Double a, Double b) {
        return b - a;
    }

    private Double times(Double a, Double b) {
        return a * b;
    }

    private Double div(Double a, Double b) {

        if (a == 0) throw new RuntimeException();
        return b / a;
    }
}
package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;


public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        if (inputNumbers.contains(null) || inputNumbers.size() > 50) throw new CannotBuildPyramidException();
        inputNumbers.sort(Comparator.naturalOrder());


        int size = inputNumbers.size();
        int height = 0;
        for (int i = 1; i < size + 1; ++i) {
            size -= i;
            height++;
        }

        if (size != 0) throw new CannotBuildPyramidException();

        int result[][] = new int[height][height * 2 - 1];
        int startPosition = height - 1;
        int listElm = 0;
        for (int i = 0; i < height; i++) {

            for (int j = 0; j < height * 2 - 1; j++) {

                result[i][j] = 0;

                if (j == startPosition) {

                    for (int k = 0; k < i + 1; k++) {

                        result[i][j] = inputNumbers.get(listElm);
                        listElm++;
                        j += 2;
                    }
                    startPosition--;
                }
            }
        }

        return result;
    }
}
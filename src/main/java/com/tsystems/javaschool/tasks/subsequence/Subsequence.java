package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public static boolean find(List x, List y) {

        if (x == null || y == null)
            throw new IllegalArgumentException();

        if (x.size() == 0)
            return true;

        LinkedList<Integer> order = new LinkedList<Integer>();
        List<Object> original = new ArrayList<>(x);
        Object currentObject = null;
        for (int i = 0; i < y.size(); i++) {

            currentObject = y.get(i);
            if (x.contains(currentObject)) {
                order.addLast(original.indexOf(currentObject));
                x.remove(currentObject);
            }
        }

        if (x.size() != 0)
            return false;
        int prevNumber = order.get(0);
        for (int i = 1; i < order.size(); i++) {

            if (prevNumber >= order.get(i))
                return false;
            prevNumber = order.get(i);

        }

        return true;
    }
}
